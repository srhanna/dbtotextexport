﻿using System;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Data;

namespace DBToTextExport
{
    class Program
    {
        static public string sCON = "server=104.168.205.130;database=MIscDB;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        //static public string sCON = "server=104.168.205.130;database=veritashome;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
        static void Main(string[] args)
        {
            ExportDBByLine();
        }

        public static void ExportDBByLine()
        {
            string filename = "cgaflcancelarch.txt";
            string SQL = "select * from cgaflcancelarch";
            string excludeColumn = "idx";
            string delimiter = "\t";
            StreamWriter writer = new StreamWriter(@"C:\repo\Files\exports\" + filename);
            using (SqlConnection conn = new SqlConnection(sCON))
            {
                using (SqlCommand command = new SqlCommand(SQL))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        command.Connection = conn;
                        adapter.SelectCommand = command;
                        using (DataTable dtEmployee = new DataTable())
                        {
                            adapter.Fill(dtEmployee);
                            string str = string.Empty;
                            foreach (DataColumn column in dtEmployee.Columns)
                            {
                                if (column.ColumnName.ToLower() == excludeColumn.ToLower())
                                    goto MoveNext;

                                // Add the header to the text file
                                str += column.ColumnName + delimiter;

                            MoveNext:;
                            }
                            str = str.Substring(0, str.Length - 1);
                            writer.WriteLine(str);
                 
                            foreach (DataRow row in dtEmployee.Rows)
                            {
                                str = string.Empty;
                                foreach (DataColumn column in dtEmployee.Columns)
                                {
                                    if (column.ColumnName.ToLower() == excludeColumn.ToLower())
                                        goto MoveNext0;
                                    // Insert the Data rows.
                                    if (row[column.ColumnName].ToString() == "")
                                        str += "" + delimiter;
                                    else
                                        if (column.ColumnName.ToString().Contains("Date") || column.ColumnName.ToString().Contains("Day"))
                                    {
                                        if (row[column.ColumnName].ToString().Length > 0)
                                            str += Convert.ToDateTime(row[column.ColumnName]).ToString("MMddyyyy") + delimiter;
                                        else
                                            str += "" + delimiter;
                                    }
                                    else
                                        str += row[column.ColumnName].ToString().Replace("\r\n", " ").Replace("\n", " ") + delimiter;

                                    MoveNext0:;
                                }
                                str = str.Substring(0, str.Length - 1);
                                writer.WriteLine(str);
                            }
                            writer.Close();
                        }
                    }
                }
            }
        }

        public static void ExportDBByString()
        {
            string filename = "claimnote.txt";
            string SQL = "select * from claimnote";
            string excludeColumn = "xid";
            string delimiter = "|";
            StreamWriter writer = new StreamWriter(@"C:\repo\Files\" + filename);
            using (SqlConnection conn = new SqlConnection(sCON))
            {
                using (SqlCommand command = new SqlCommand(SQL))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter())
                    {
                        command.Connection = conn;
                        adapter.SelectCommand = command;
                        using (DataTable dtEmployee = new DataTable())
                        {
                            adapter.Fill(dtEmployee);
                            string str = string.Empty;
                            foreach (DataColumn column in dtEmployee.Columns)
                            {
                                if (column.ColumnName == excludeColumn)
                                    goto MoveNext;

                                // Add the header to the text file
                                str += column.ColumnName + delimiter;
                            MoveNext:;

                            }
                            // Insert a new line.
                            str = str.Substring(0, str.Length - 1);
                            str += "\r\n";
                            foreach (DataRow row in dtEmployee.Rows)
                            {
                                foreach (DataColumn column in dtEmployee.Columns)
                                {
                                    if (column.ColumnName == excludeColumn)
                                        goto MoveNext0;
                                    // Insert the Data rows.
                                    if (row[column.ColumnName].ToString() == "")
                                        str += "" + delimiter;
                                    else
                                        str += row[column.ColumnName].ToString().Replace("\r\n", " ") + delimiter;

                                    MoveNext0:;
                                }
                                str = str.Substring(0, str.Length - 1);
                                // Insert a  new line.
                                str += "\r\n";
                            }
                            writer.Write(str);
                            writer.Flush();
                        }
                    }
                }
            }
        }

    }

}
    

